<?php get_header();?>

<section class="single_breadcrumb">
 <div class="container">
  <div class="breadcrumb_wrapper"><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></div>
</div>
</section>

<section class="single_page_section">
	<div class="container">
    <div class="the_title_wrapper">
      <h1 class="single_title"><?php echo get_the_title(); ?></h1>
      <div class="post-byline u-m-1_5">
           <p>
            by <a class="post-authorName" href="#"><?php global $post;  echo  get_the_author_meta( 'nicename',(int)$post->post_author ); ?></a>
            — <time  class="timeago" ><?php echo get_the_date(); ?></time> 
           </p>
    </div>
    </div>
		<div class="the_content_wrapper">
			<?php
	        // Start the loop.
	        while ( have_posts() ) : the_post();
	         the_content();
	        // End the loop.
	        endwhile;
	        ?>
		</div>
    
    <div class="subform_wrapper">

      <div class="subform">
        <div class="subform_title">
          <h6>Get "AWESOME Gadget" Recommendations Straight to Your Inbox</h6>
        </div>
        <form action="<?php echo get_template_directory_uri()?>/mails/mail.php" method="POST" name="subcription">
          <input type="email" placeholder="Email" required="" name="semail">
          <input type="submit" id="btn" placeholder="Email" name="submit" value="Subcribe">
        </form>
      </div>
    </div>

		<div class="the_sidebar_wrapper">
	      
              <div class="tabs">
                <div class="tab">
                  <input type="radio" name="css-tabs" id="tab-1" checked class="tab-switch">
                  <label for="tab-1" class="tab-label">Popular Posts</label>
                  <div class="tab-content">
                     
                    <ul id="slider-id" class="sidebar-ul">
                      <?php
                      $fields = get_field( "popluar_post", 5 );
                      foreach($fields as $post) : ?>
                          <li>
                              <a href="<?php echo get_permalink( $post['post'] ); ?>">
                                  <p class="slider-caption-class"><?php echo get_the_title($post['post']); ?></p>
                              </a>
                          </li>
                      <?php endforeach; wp_reset_query(); ?>
                  </ul>

                   </div>
                </div>
                <div class="tab">
                  <input type="radio" name="css-tabs" id="tab-2" class="tab-switch">
                  <label for="tab-2" class="tab-label">Recent Posts</label>
                  <div class="tab-content">
                    
                    <ul id="slider-id" class="sidebar-ul">
                      <?php
                      $recent_posts = wp_get_recent_posts(array(
                          'numberposts' =>4, // Number of recent posts thumbnails to display
                          'post_status' => 'publish' // Show only the published posts
                      ));
                      foreach($recent_posts as $post) : ?>
                          <li>
                              <a href="<?php echo get_permalink($post['ID']) ?>">
                                 
                                  <p class="slider-caption-class"><?php echo $post['post_title'] ?></p>
                              </a>
                          </li>
                      <?php endforeach; wp_reset_query(); ?>
                  </ul>


                  </div>
               </div>
            </div>
				</div>

        <div class="catlist_wrapper">
          <div class="catlist_title"><h2>Categories</h2></div>
          
          <?php 

          $categories = get_categories( array(
              'orderby' => 'name',
              'order'   => 'ASC'
          ) );
           
          foreach( $categories as $category ) { ?>
                       <a href="<?php echo get_category_link($category->cat_ID); ?>"><h3 class="sidebar_cat_title"><?php echo $category->name; ?></h3></a>
                       <ul id="slider-id" class="sidebar-ul">
                                <?php
                                $recent_posts = wp_get_recent_posts(array(
                                    'cat'=>$category->cat_ID,
                                    'numberposts' =>3, // Number of recent posts thumbnails to display
                                    'post_status' => 'publish' // Show only the published posts
                                ));
                                foreach($recent_posts as $post) : ?>
                                    <li>
                                        <a href="<?php echo get_permalink($post['ID']) ?>">
                                           
                                            <p class="slider-caption-class"><?php echo $post['post_title'] ?></p>
                                        </a>
                                    </li>
                                <?php endforeach; wp_reset_query(); ?>
                            </ul>

                      <?php } ?>

        </div>


		</div>
	</div>
</section>

<?php get_footer(); ?>







   