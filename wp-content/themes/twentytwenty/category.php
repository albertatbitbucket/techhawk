<?php 
get_header();

$category = get_queried_object();  ?>

<!-- <p>Category: <?php single_cat_title(); ?></p> -->
<?php 
// $args = array ( 'category' => $category->cat_ID, 'posts_per_page' => 5);
// $myposts = get_posts( $args );
// foreach( $myposts as $post ) :	
// var_dump( $post->post_title);
 ?>
<?php //endforeach; ?>

<?php 
  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  $cat_id=$category->cat_ID;
  query_posts('cat='.$cat_id.'&posts_per_page=4&paged='.$paged); //<-- set cat= to the numeric category

?>

             

<section class="cat_title">
 <div class="container">
	<div class="breadcrumb_wrapper"><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></div>
  
</div>
</section>

<section class="section_cat">
	<div class="container">

    <div class="cat_title "><h2><?php single_cat_title(); ?></h2></div>

		<div class="content_cat">

               <?php 

              if ( have_posts() ) : 
                  while ( have_posts() ) : the_post();  ?>

                      <div class="single_content_cat">
                             <div class="single_wrapper">
                             <a href="#">
                        <?php  if ( has_post_thumbnail('medium_large') ) {
                                    the_post_thumbnail('medium_large');
                                }
                                else {
                                    echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) 
                                        . '/images/thumbnail-default.jpg" />';
                                }  ?>
  
                             </a>
                             <h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php  the_title(); ?></a></h3>
                              <span class=""><time class="" datetime="2020-08-06T00:00:00+05:30">August 6, 2020</time></span>
                             <?php the_excerpt(); ?>
                             </div>
                      </div>

                      <?php
                  endwhile; 
              else: 
                  _e( 'Sorry, no posts matched your criteria.', 'textdomain' ); 
              endif; 

               ?>
                 
			  
            <div class="pagination_wrapper">
              <div class="pagination"><?php next_posts_link('Next Page &raquo;') ?></div>
              <div class="pagination"><?php previous_posts_link('&laquo; Previous Page') ?></div>
            </div>
          


		</div>

        <div class="sbar_right">

		<div class="subform_wrapper b-shadow">

			<div class="subform">
				<div class="subform_title">
					<h6>Get "AWESOME Gadget" Recommendations Straight to Your Inbox </h6>
				</div>
				<form action="<?php echo get_template_directory_uri()?>/mails/mail.php" method="POST" name="subcription">
					<input type="email" placeholder="Email" required="" name="semail">
					<input type="submit" id="btn" placeholder="Email" name="submit" value="Subcribe">
				</form>
			</div>
		</div>
          
        <div class="recent_p_wrapper b-shadow">
        	<div class="recent_post">
            <div class="recent_post_group">
        		  
               <div class="tabs">
                <div class="tab">
                  <input type="radio" name="css-tabs" id="tab-1" checked class="tab-switch">
                  <label for="tab-1" class="tab-label">Popular Posts</label>
                  <div class="tab-content">
                     
                    <ul id="slider-id" class="sidebar-ul">
                      <?php
                      $recent_posts = wp_get_recent_posts(array(
                          'numberposts' => 4, // Number of recent posts thumbnails to display
                          'post_status' => 'publish' // Show only the published posts
                      ));
                      foreach($recent_posts as $post) : ?>
                          <li>
                              <a href="<?php echo get_permalink($post['ID']) ?>">
                                 
                                  <p class="slider-caption-class"><?php echo $post['post_title'] ?></p>
                              </a>
                          </li>
                      <?php endforeach; wp_reset_query(); ?>
                  </ul>

                   </div>
                </div>
                <div class="tab">
                  <input type="radio" name="css-tabs" id="tab-2" class="tab-switch">
                  <label for="tab-2" class="tab-label">Recent Posts</label>
                  <div class="tab-content">
                    
                    <ul id="slider-id" class="sidebar-ul">
                      <?php
                      $recent_posts = wp_get_recent_posts(array(
                          'numberposts' =>2, // Number of recent posts thumbnails to display
                          'post_status' => 'publish' // Show only the published posts
                      ));
                      foreach($recent_posts as $post) : ?>
                          <li>
                              <a href="<?php echo get_permalink($post['ID']) ?>">
                                 
                                  <p class="slider-caption-class"><?php echo $post['post_title'] ?></p>
                              </a>
                          </li>
                      <?php endforeach; wp_reset_query(); ?>
                  </ul>


                  </div>
                </div>
             
              </div>
             
        	</div>

        	</div>
        </div> 

       </div>

	</div>
</section>


<?php get_footer(); ?>

