<!-- Banner Starts -->
<section class="banner">
	<div class="container bannerdiv">
    <div class="text-box"> 
      <h1><span class="title-primary-main">Find The Best Products</span> <span class="title-primary-sub">That Suit Your Needs</span></h1>
		<button id="mainbtn">GET STARTED</button>
    </div>
	</div>
</section>
<!-- Banner Ends -->