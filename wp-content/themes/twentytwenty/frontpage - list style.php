<?php
/*

Template Name: Homepage

*/

get_header(); ?>


<?php

$background_color = get_background_color();
 ?>

<style type="text/css">


	.container{
		width: 95%;
		margin: auto;
	} 

    /*Social media icons stars*/
	.sm,.sm ul{
		padding: 0;
		margin: 0;
		background-color: #111111;
	}
   
    .smcontainer{

     padding: 5px 8% 5px 5px;
    }

	.sm ul li{
		display: block;
	}
	 .smul{
	 	text-decoration: none;
	 	list-style: none;
	 	display: flex;
	 	flex-direction: row;
	 	justify-content: flex-end;
	 }
     .smimage{
     	/*mix-blend-mode: multiply;*/
     	width: auto;
     	height: 20px;
     }
     img.smimage:hover {
       transform: scale(1.2);
     }
     /*Social media icons ends*/


     .header-inner{
     	padding-top: 0px;  /*To avoid the defalut padding */
     }
       
      /*Menu editing starts*/ 
     .menu-item a {
	    color: white !important; /*Overriding the default menu color*/
	 }
      
      .menu-item a:hover {
       color: gray!important;
       /*transform: scale(1.1);*/
      }
      /*Menu editing ends*/ 
	 
      /*Banner section start here*/ 

	.bannerdiv{
		min-height: 400px;
		padding-top: 100px;
	}

	.bannerdiv h1{
		margin: 0;
		font-size: 40px;
		color: white;
		text-align: center;
		line-height: 60px;
        filter: drop-shadow(0 0 3.75rem black);
	} 

	.bannerdiv h2{
		margin: 0;
		font-size: 30px;
		color: white;
		text-align: center;
		line-height: 60px;
		filter: drop-shadow(0 0 3.75rem black);
       
	} 
	.banner{
		padding: 3rem 0;
		background-image: url("<?php echo get_template_directory_uri(); ?>/images/banner.jpg");
		background-position: top;
		
		filter: contrast(1);
	}
	#mainbtn{
		width: 250px;
		position: relative;
		left: 50%;
		transform: translate(-50%, 0%);
		background-color: transparent;
		border: 2px solid white;
		text-decoration: none;
		filter: drop-shadow(0 0 2.75rem black);
	}

	#mainbtn:focus{
		outline: none;
		border: none;
		border: 2px solid white;
		
	}

	#mainbtn:hover{
		border: 2px solid black;
		
    }

    /*Banner section ends here*/ 

    /*Content section start here*/
    .content{
	 background-color: <?php echo $background_color; ?>;
	}
	.contents{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		padding: 10px;

	}

	.contents .home-category-box{
		margin: 0 auto 0 auto;
		width: 50%;
		min-height: 310px;
		padding: 20px;
	}
	.contents .home-category-box h2{
		margin-top: 20px;
        padding-bottom: 20px;
        border-bottom: 0.5px solid;
		font-size: 26px;
		position: relative;
		left:6%;
	}
    .contents .home-category-box h2 a{

      color: black !important;
    }

    .contents .home-category-box h2 a svg{
    	float: left;
      display: block;
	  margin-left: 20px;
	  content: ' ';
	  background-image: url("http://localhost/techhawk/wp-content/themes/twentytwenty/images/ironbox.svg");
	  background-size: 28px 28px;
	  height: 28px;
	  width: 28px;
	  background-color: white;
	}


    .home-category-box ul li a{
    	 color: black !important;
    	 display: block;
    	 float: left;
    }
	.home-category-box ul li {
	 color: black !important;
     float: left;
     width: 224.297px;
     }

      .svg{
	  width: 25px;
	  height: 25px;
	  display: inline;
	  margin-left: 9px;
	  }
  /*Content section start here*/

  
  /*Site footer edit*/

.footer-credits {
    /* display: flex; */
    position: relative;
    left: 50%;
    transform: translate(-50%, 0);
}
#site-footer{
	border-top: 0.5px solid;
}

  /*Site footer edit*/



.clearfix:after {
    content: "";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
  }

/*  Media Queries Starts Here*/

  @media (min-width: 320px) and (max-width: 480px) {

 
       .bannerdiv{
       	 padding-top: 60px !important;
       }

       .bannerdiv h1{
		
		line-height: 50px;
		font-size: 30px;
		
	} 

	.bannerdiv h2{
		 
		 line-height: 50px;
		font-size: 20px;
		
       
	}
	#mainbtn{
		width: 200px;
	}
	 .contents{
	
		flex-direction: column;
		

	}
	.contents .home-category-box{
		width: 100%;
	}
    
    .smcontainer {
    padding: 5px 2% 5px 5px;
    } 
	  
	}


/* iPads (portrait and landscape) ----------- */
@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px) {

}

/* iPads (landscape) ----------- */
@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
and (orientation : landscape) {

}

/* (640x960) iPhone 4 & 4S */
@media only screen and (min-device-width: 640px) and (max-device-width: 960px) {
   .smcontainer {
    padding: 5px 2% 5px 5px;
    }
}

@media only screen and (min-device-width: 480px) and (max-device-width: 800px) {
    .smcontainer {
    padding: 5px 2% 5px 5px;
    }
}

/*  Media Queries Ends Here*/
</style>


<!-- Banner Starts -->
<section class="banner">
	<div class="container bannerdiv">
		<h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h1>
		<h2>Sit amet, consectetur adipisicing elit</h2>
		<button id="mainbtn">GET STARTED</button>
	</div>
</section>
<!-- Banner Ends -->

<!-- Main Starts -->
<section class="content">
<div class="container contents">

<div class="home-category-box">
<h2 class="clearfix"><a href="#">Best Home Appliances <img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/ironbox.svg" alt=""><img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/homea.svg" alt=""></a></h2>
<ul>
<li><a href="https://www.techhawk.in/best-ac-india/">Best ACs in India</a></li>
<li><a href="https://www.techhawk.in/best-air-coolers/">Best Air Coolers</a></li>
<li><a href="https://www.techhawk.in/best-washing-machines/">Best Washing Machines</a></li>
<li><a href="https://www.techhawk.in/best-vacuum-cleaners/">Best Vacuum Cleaners</a></li>
<li><a href="https://www.techhawk.in/best-cctv-cameras/">Best CCTV Cameras</a></li>
<li><a href="https://www.techhawk.in/best-inverters/">Best Inverters</a></li>
<li><a href="https://www.techhawk.in/best-air-purifiers/">Best Air Purifiers</a></li>
<li><a href="https://www.techhawk.in/best-room-heaters/">Best Room Heaters</a></li>
<li><a href="https://www.techhawk.in/best-water-heaters/">Best Water Heaters</a></li>
<li><a href="https://www.techhawk.in/best-sewing-machines/">Best Sewing Machines</a></li>
<li><a href="#">View All &gt;&gt;</a></li>
</ul>
</div>


<div class="home-category-box">
<h2><a href="#">Best Travel & Fitness <img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/travel.svg" alt=""><img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/fitness.svg" alt=""></a></h2>
<ul>
<li><a href="https://www.techhawk.in/best-ac-india/">Best ACs in India</a></li>
<li><a href="https://www.techhawk.in/best-air-coolers/">Best Air Coolers</a></li>
<li><a href="https://www.techhawk.in/best-washing-machines/">Best Washing Machines</a></li>
<li><a href="https://www.techhawk.in/best-vacuum-cleaners/">Best Vacuum Cleaners</a></li>
<li><a href="https://www.techhawk.in/best-cctv-cameras/">Best CCTV Cameras</a></li>
<li><a href="https://www.techhawk.in/best-inverters/">Best Inverters</a></li>
<li><a href="https://www.techhawk.in/best-air-purifiers/">Best Air Purifiers</a></li>
<li><a href="https://www.techhawk.in/best-room-heaters/">Best Room Heaters</a></li>
<li><a href="https://www.techhawk.in/best-water-heaters/">Best Water Heaters</a></li>
<li><a href="https://www.techhawk.in/best-sewing-machines/">Best Sewing Machines</a></li>
<li><a href="#">View All &gt;&gt;</a></li>
</ul>
</div>

<div class="home-category-box">
<h2><a href="#">Best Electronics <img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/ele.svg" alt=""><img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/ele2.svg" alt=""></a></h2>
<ul>
<li><a href="https://www.techhawk.in/best-ac-india/">Best ACs in India</a></li>
<li><a href="https://www.techhawk.in/best-air-coolers/">Best Air Coolers</a></li>
<li><a href="https://www.techhawk.in/best-washing-machines/">Best Washing Machines</a></li>
<li><a href="https://www.techhawk.in/best-vacuum-cleaners/">Best Vacuum Cleaners</a></li>
<li><a href="https://www.techhawk.in/best-cctv-cameras/">Best CCTV Cameras</a></li>
<li><a href="https://www.techhawk.in/best-inverters/">Best Inverters</a></li>
<li><a href="https://www.techhawk.in/best-air-purifiers/">Best Air Purifiers</a></li>
<li><a href="https://www.techhawk.in/best-room-heaters/">Best Room Heaters</a></li>
<li><a href="https://www.techhawk.in/best-water-heaters/">Best Water Heaters</a></li>
<li><a href="https://www.techhawk.in/best-sewing-machines/">Best Sewing Machines</a></li>
<li><a href="#">View All &gt;&gt;</a></li>
</ul>
</div>

<div class="home-category-box">
<h2><a href="#">Best Fashion <img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/fashion.svg" alt=""><img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/fashion1.svg" alt=""></a></h2>
<ul>
<li><a href="https://www.techhawk.in/best-ac-india/">Best ACs in India</a></li>
<li><a href="https://www.techhawk.in/best-air-coolers/">Best Air Coolers</a></li>
<li><a href="https://www.techhawk.in/best-washing-machines/">Best Washing Machines</a></li>
<li><a href="https://www.techhawk.in/best-vacuum-cleaners/">Best Vacuum Cleaners</a></li>
<li><a href="https://www.techhawk.in/best-cctv-cameras/">Best CCTV Cameras</a></li>
<li><a href="https://www.techhawk.in/best-inverters/">Best Inverters</a></li>
<li><a href="https://www.techhawk.in/best-air-purifiers/">Best Air Purifiers</a></li>
<li><a href="https://www.techhawk.in/best-room-heaters/">Best Room Heaters</a></li>
<li><a href="https://www.techhawk.in/best-water-heaters/">Best Water Heaters</a></li>
<li><a href="https://www.techhawk.in/best-sewing-machines/">Best Sewing Machines</a></li>
<li><a href="#">View All &gt;&gt;</a></li>
</ul>
</div>

<div class="home-category-box">
<h2><a href="#">Best Health & Beauty <img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/health.svg" alt=""><img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/beauty.svg" alt=""></a></h2>
<ul>
<li><a href="https://www.techhawk.in/best-ac-india/">Best ACs in India</a></li>
<li><a href="https://www.techhawk.in/best-air-coolers/">Best Air Coolers</a></li>
<li><a href="https://www.techhawk.in/best-washing-machines/">Best Washing Machines</a></li>
<li><a href="https://www.techhawk.in/best-vacuum-cleaners/">Best Vacuum Cleaners</a></li>
<li><a href="https://www.techhawk.in/best-cctv-cameras/">Best CCTV Cameras</a></li>
<li><a href="https://www.techhawk.in/best-inverters/">Best Inverters</a></li>
<li><a href="https://www.techhawk.in/best-air-purifiers/">Best Air Purifiers</a></li>
<li><a href="https://www.techhawk.in/best-room-heaters/">Best Room Heaters</a></li>
<li><a href="https://www.techhawk.in/best-water-heaters/">Best Water Heaters</a></li>
<li><a href="https://www.techhawk.in/best-sewing-machines/">Best Sewing Machines</a></li>
<li><a href="#">View All &gt;&gt;</a></li>
</ul>
</div>

<div class="home-category-box">
<h2><a href="#">Best Home Appliances  <img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/health.svg" alt=""><img class="svg" src="http://localhost/techhawk/wp-content/themes/twentytwenty/images/beauty.svg" alt=""></a></h2>
<ul>
<li><a href="https://www.techhawk.in/best-ac-india/">Best ACs in India</a></li>
<li><a href="https://www.techhawk.in/best-air-coolers/">Best Air Coolers</a></li>
<li><a href="https://www.techhawk.in/best-washing-machines/">Best Washing Machines</a></li>
<li><a href="https://www.techhawk.in/best-vacuum-cleaners/">Best Vacuum Cleaners</a></li>
<li><a href="https://www.techhawk.in/best-cctv-cameras/">Best CCTV Cameras</a></li>
<li><a href="https://www.techhawk.in/best-inverters/">Best Inverters</a></li>
<li><a href="https://www.techhawk.in/best-air-purifiers/">Best Air Purifiers</a></li>
<li><a href="https://www.techhawk.in/best-room-heaters/">Best Room Heaters</a></li>
<li><a href="https://www.techhawk.in/best-water-heaters/">Best Water Heaters</a></li>
<li><a href="https://www.techhawk.in/best-sewing-machines/">Best Sewing Machines</a></li>
<li><a href="#">View All &gt;&gt;</a></li>
</ul>
</div>

</div>	
</section>

<!-- Main Ends -->

<?php get_footer(); ?>

<script type="text/javascript">
	document.getElementById("mainbtn").onclick=function(){
		 var body = jQuery('html, body');
        body.animate({scrollTop:600}, 500, 'swing');
	}
	
</script>
<!-- , sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum. -->