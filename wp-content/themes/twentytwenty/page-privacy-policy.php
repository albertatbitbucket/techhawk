<?php get_header();  ?>

<section class="single_breadcrumb">
 <div class="container">
  <div class="breadcrumb_wrapper"><?php if (function_exists('the_breadcrumb')) the_breadcrumb(); ?></div>
</div>
</section>

<section class="section_privacy_policy">
	<div class="container">
	    <div class="the_title_wrapper">
	      <h1 class="single_title"><?php echo get_the_title(); ?></h1>
	    </div>
	    <div class="p_the_content_wrapper">
			<?php
	        // Start the loop.
	        while ( have_posts() ) : the_post();
	         the_content();
	        // End the loop.
	        endwhile;
	        ?>
		</div>
   </div>
</section>

<?php get_footer(); ?>