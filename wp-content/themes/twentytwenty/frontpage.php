<?php
/*

Template Name: Homepage

*/

get_header(); ?>

<?php get_template_part('template-parts/banner'); ?>

<!-- Main Starts -->
   <section class="content">
      <div class="container maincontent">


<?php 

$categories = get_categories( array(
    'orderby' => 'name',
    'order'   => 'ASC'
) );
 
foreach( $categories as $category ) { ?>

    <div class="maincontent_wrapper">
            <div class="title_wrapper">
               <h1 class="title_cat"><?php echo $category->name; ?></h1>  
            </div>
             <div class="content_wrapper">

     <?php query_posts('cat='.$category->cat_ID.'&posts_per_page=4&paged='.$paged);

     if ( have_posts() ) : 
                  while ( have_posts() ) : the_post();  $title=get_the_title(); 
                    if(strlen($title)>=70){ $title = substr($title,0,50).'...'; } ?>
                     
                      <div class="single_content">
                             <div class="single_wrapper">
                             <a href="#">
                        <?php  if ( has_post_thumbnail('medium_large') ) {
                                    the_post_thumbnail('medium_large');
                                }
                                else {
                                    echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) 
                                        . '/images/thumbnail-default.jpg" />';
                                }  ?>
  
                             </a>
                             <h3 class="entry-title td-module-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php  echo $title; ?></a></h3>
                              <span class=""><time class="" datetime="2020-08-06T00:00:00+05:30"><?php echo get_the_date(); ?></time></span>
                             <?php the_excerpt(); ?>
                             </div>
                      </div>

                      <?php
                  endwhile; 
              else: 
                  _e( 'Sorry, no posts matched your criteria.', 'textdomain' ); 
              endif; 
      
      ?>
   
     <div class="btnexclude"><a class="plusbtn" href="<?php echo get_category_link($category->cat_ID); ?>"><svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="128px" height="128px"><path d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/><path d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/></svg></a></div>
       </div>
    </div>  <!-- Main content ends -->

  <?php } ?>


           

         </div>
      </div>   
   </section>

<!-- Main Ends -->

<?php get_footer(); ?>

<script type="text/javascript">
	document.getElementById("mainbtn").onclick=function(){
		 var body = jQuery('html, body');
        body.animate({scrollTop:document.getElementsByClassName("content")[0].offsetTop}, 500, 'swing');
	}
	
</script>
<!-- , sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum. -->