<?php

/**
 * Fired during plugin deactivation
 *
 * @link       albert davis
 * @since      1.0.0
 *
 * @package    Booking
 * @subpackage Booking/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Booking
 * @subpackage Booking/includes
 * @author     albert davis <albertdavis.online@gmail.com>
 */
class Booking_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
