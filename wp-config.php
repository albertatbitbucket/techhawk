<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'techhawk' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[uQ-R$Y!ZCKM{)(k%b5dm};eRkC&B?woo?o.wFvf22)7BN]`{,s/pH6*>g;@d,cm' );
define( 'SECURE_AUTH_KEY',  's`L0SS7kiUhA.LH7+TFlgq^Y)yVQ94+Uinpbx)u7_2!70Mv7qU.M)J7}W0KmPc<k' );
define( 'LOGGED_IN_KEY',    '(K?{<&zKtqCaZP[dPDU(:^6+;(1*M#uX/1cT@V;n4]|JM9kW/t.p$Aj)*S>g(T%V' );
define( 'NONCE_KEY',        'C3tI}{ $94dR7oKCV4V6$l=bGt]!YC033o.^j&kj;@Z9U25U#J@ZQI $7$f!nq*F' );
define( 'AUTH_SALT',        'G9b_7U-G #Mi^I3H>yxRZ;0>s%J@mVL43+Qs-MLjRA)!G/+|y!jWiElP+6;1mI-H' );
define( 'SECURE_AUTH_SALT', '{no8MjK7KLaNdAbD^xo5SeI1hi.Ke/PM(MMzB`$RWNwktU&ch,>DPmm^y<lu`1[5' );
define( 'LOGGED_IN_SALT',   'HJIF+A17<`k^)>LGvMs>#||!#)6B?)_OY$n)}2xD_Ds*O/*.*#jH{+:UX.tIK@;/' );
define( 'NONCE_SALT',       '9/H|e|*7$.^|)XP,06(Q%Wq&866iv>p;Y T3?lO~ >#eS9,ei?pmnl`1RhXPJ>h}' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'th_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
